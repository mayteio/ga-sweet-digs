<?php get_header() ?>

    <section class="main" role="main">
      <div class="row">
        <div class="small-12 medium-8 end columns">
          <h1>We create beautiful websites using WordPress <i class="fa fa-angellist"></i></h1>
          <?php wp_nav_menu(array('menu_class' => 'site-links')) ?>
        </div>
      </div>
    </section>

<?php get_footer() ?>
