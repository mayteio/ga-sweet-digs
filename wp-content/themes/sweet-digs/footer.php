<footer>
  <span class="copyright">
    &copy; <?php bloginfo('name') ?> '<?php echo date('y') ?></span>

  <a href="<?php bloginfo('url') ?>/contact" class="green float-right">Contact Us</a>
</footer>

<script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2dc609b09a.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/app.js"></script>

<?php wp_footer() ?>
</body>
</html>
