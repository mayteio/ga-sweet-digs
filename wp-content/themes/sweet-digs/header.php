<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php wp_title() ?></title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/6.2.3/foundation.min.css">
    <link href='https://fonts.googleapis.com/css?family=Karla:400,700|Playfair+Display:700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">

    <?php wp_head() ?>


  </head>
  <body <?php body_class() ?>>

    <header>
      <a class="logo" href="<?php bloginfo('url') ?>"><?php bloginfo('name') ?> <i class="fa fa-angellist"></i></a>
      <a class="toggle-menu float-right"><i class="fa fa-bars"></i></a>
    </header>

    <nav class="side-menu">
      <a class="toggle-menu float-right"><i class="fa fa-times"></i></a>
      <?php wp_nav_menu(array('menu_class' => 'site-links')) ?>
    </nav>
