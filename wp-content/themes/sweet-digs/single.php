<?php get_header() ?>


    <section class="main" role="main">
      <div class="row">
        <div class="small-12 medium-8 columns">
          <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post() ?>

              <article class="blog-post">
                <h2>

                    <?php the_title() ?>

                </h2>
                <?php the_content() ?>
                <p class="byline">
                  By <?php the_author() ?>
                  on <?php the_time('jS F Y') ?>
                </p>
              </article>

              <?php comments_template() ?>

            <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="small-12 medium-4 columns">
          <?php dynamic_sidebar() ?>
        </div>
      </div>
    </section>


<?php get_footer() ?>
