# Create your own WordPress theme

G'day there! We'll be running through how to create a WordPress theme from scratch, covering the basics. Below are a few resources that will help along the way.

## Handy links
* Related files: https://www.dropbox.com/sh/rxdj05gdy13x44m/AACsP0YXpOI98bay93DHKAQCa?dl=0
* XAMPP: https://www.apachefriends.org/download.html
* WordPress: https://wordpress.org
* WordPress Documentation: http://codex.wordpress.org
* Free CSS/Web font Icons: http://fontawesome.io
* Free Web fonts: http://fonts.google.com
* Zurb Foundation CSS Framework: http://foundation.zurb.com/docs
* Ask anything (but search first): http://wordpress.stackexchange.com
* MAMP (awesome but large alternative to XAMPP): https://www.mamp.info/en/
* (Advanced) Roots.io stack. Check out Sage: https://roots.io/


## Gravity Forms
* Download URL: https://www.dropbox.com/s/kz54lz7iuipctaj/gravityforms_2.0.5.zip?dl=0
* License Key: `5b17326e36b1b72ddda9cf48f37dd9a5`

## Basic HTML Boilerplate

```
<!-- index.php -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php wp_title() ?></title>

    <?php wp_head() ?>

    <!-- Stylesheets go here -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/6.2.3/foundation.min.css">
    <link href='https://fonts.googleapis.com/css?family=Karla:400,700|Playfair+Display:700' rel='stylesheet' type='text/css'>

  </head>
  <body <?php body_class() ?>>
    <!-- Site code goes here -->
    <h1>Dayum this is some sweet digs.</h1>



    <!-- Scripts go below here -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2dc609b09a.js"></script>

    <script>
      // Javascript goes here
    </script>

    <?php wp_footer() ?>

  </body>
</html>

<!-- style.css -->
/*
  Theme Name: Sweet Digs
*/
```

## All files related to workshop:
Download from Dropbox: https://www.dropbox.com/sh/rxdj05gdy13x44m/AACsP0YXpOI98bay93DHKAQCa?dl=1

## Further learning
* Tuts+ - http://code.tutsplus.com/categories/wordpress
* WPMUDEV - https://premium.wpmudev.org/blog/
* Books - http://www.wpbeginner.com/category/wp-tutorials/


## Contact me if you have any questions!
harley@mayte.io