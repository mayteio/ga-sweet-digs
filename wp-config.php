<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// Damn XAMPP
define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',D:`vfb[13^UgBTf~mzU+dJevhY]yfFoB5!ZQH45NNx*R%L+>+N^EAWMqc3W#)~v');
define('SECURE_AUTH_KEY',  '2oInp.30j:]*C4Qp@(0Y]ZQ~kmMG()mBor=5hv6[QXYtj}~z~v-LxQF*kNdJ=s<Z');
define('LOGGED_IN_KEY',    'WK[u^Gp@FXB[N4<1JIfr6SX[,6MHUms)N_x},v<<Y74u<Jq@x[|k^8o:QZ0~:$`9');
define('NONCE_KEY',        '!hN]kmQ>3aF(OY&GB }Fb-MCV7}{&1IM+WvJ_}EO%6Htid@&~_G/xx<}u,=*rf-N');
define('AUTH_SALT',        'A#I;I[4GS9H0}X/6f:YvbsY M_%f#+xrrXqTnLx.hBKz<_0nk(8{ >J|!kS{UtSt');
define('SECURE_AUTH_SALT', '8mg-f,0P=k2~L|nQVXKNT4mN7q@N3..f)[;-k/G9N.hcTW:<8g7KY#X~yc~U&d^{');
define('LOGGED_IN_SALT',   'XMBG&h t@mJpL[:9[_AOa[Ttj;e;pT>-R%6e& =#9W&>`NTiuz[]kI_s2UTXI;xO');
define('NONCE_SALT',       'uK;gP.DJclCee#:DiWTG0YyXTic|!h tCv//1#9eT7aUV}mYm]A,FVp8D%)xw5L9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
